#Trainup Styling - v1.0.0

<br>

##General description
<br>
This exercise is all about improving code of styles while not changing anything in HTML files.

##IDE recommendation
<br>
It is recommended to use VS Code with live server extension to make your live easier but you can use any IDE you like, it's up to you! :)
